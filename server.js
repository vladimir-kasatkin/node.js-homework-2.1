const http = require('http');
const querystring = require('querystring');
const port = 3000;

// создаем POST запрос на сосновании введенных от пользователя имени и фамилии
function create_request(firstname, content_length) {
  console.log('build reuest for netology.tomilomark.ru ... ');
  let options = {
  hostname: 'netology.tomilomark.ru',
  path: '/api/v1/hash',
  method: 'POST',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'firstname': firstname,
    'Content-Length': content_length
  }
  };
  return options;
}

// получаем ответ от сервиса генерации ключей и генерируем ответ
function handler(response, res, firstname, lastname ) {
  let data = '';
  response.on('data', function (chunk) {
    data += chunk;
  });
  // есть параметры от клиента, есть секретный ключ, все готово чтобы отдать данные клиенту.
  response.on('end', function () {
    console.log('secret data is  collected');
    res.writeHead(200, 'OK', {'Content-Type': 'application/json'});
    JSONdata = JSON.parse(data);
    JSONdata.firstName = firstname;
    JSONdata.lastName = lastname;
    res.write(JSON.stringify(JSONdata));
    res.end();
  });
}

// обработка запросов: при получении данных сделать внутренний запрос - на tomilomark.ru
function server_handler(req, res) {
  let data = '';
  req.on('data', chunk => data += chunk);
  req.on('end', () => {
    // парсим параметры от клиента и отправляем их же дальше, на получение ключа
    let params = querystring.parse(data);
    let secret_request_data = querystring.stringify({'lastName': params.lastname});
    let request = http.request(create_request( params.firstname, Buffer.byteLength(secret_request_data) ));
    request.on('response', (response) => handler(response, res, params.firstname, params.lastname ));
    request.write(secret_request_data);
    console.log('send secret request to netology.tomilomark.ru ... ');
    request.end();

  });
}

 // созадем сервер, который слушает порт, и если поступает запрос - обрабатывает
const server = http.createServer();
server.on('error', err => console.error(err));
server.on('request', server_handler);
server.on('listening', () => {
  console.log('Start HTTP on port %d', port);
});
server.listen(port);
