const querystring = require('querystring');

let data = querystring.stringify({
  'firstname': process.argv[2],
  'lastname': process.argv[3]
});

let options = {
  hostname: '127.0.0.1',
  port: 3000,
  method: 'POST',
  headers: {
  'Content-Type': 'application/x-www-form-urlencoded',
  'Content-Length': Buffer.byteLength(data)
  }
};

function handler(response) {
  let data = '';
  response.on('data', function (chunk) {
    data += chunk;
  });
  response.on('end', function () {
    console.log(data);
  });
}

// check args
if (!((process.argv[2]) && (process.argv[3]))) {
    console.log(" Usage: client.js firstname lastname");
    return;
}

//create request to server
const http = require('http');
let request = http.request(options);
request.write(data);
request.on('response', handler);
request.end();
